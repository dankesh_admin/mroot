/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util


import wang.encoding.mroot.common.constant.Global
import wang.encoding.mroot.common.exception.BaseException
import java.io.*
import java.util.*

/**
 * 文件工具类
 * @author ErYang
 */
object FileUtil {

    /**
     * 点名称
     */
    private const val DOT: String = "."

    /**
     * 点名称
     */
    private const val SIZE_2MB: Long = 1024 * 1024 * 2

    /**
     * 根据文件名 获取其后缀信息
     *
     * @param fileSize 文件大小
     * @return String
     */
    @JvmStatic
    fun gt2Mb(fileSize: Long): Boolean {
        return fileSize > SIZE_2MB
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据文件名 获取其后缀信息
     *
     * @param filename 文件名
     * @return String
     */
    @JvmStatic
    fun getSuffixByFilename(filename: String): String {
        return filename.substring(filename.lastIndexOf(DOT) + 1).toLowerCase()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建文章上传图片路径
     *
     * @return 文件夹路径
     */
    @JvmStatic
    fun createUploadImgPath2Article(uploadPath: String): String {
        // 文章上传图片路径
        return createPath(uploadPath + Global.ARTICLE_IMG + FileUtil.getDateFormat()).path
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 只删除此路径的最末路径下所有文件和文件夹
     *
     * @param folderPath 文件路径
     */
    fun delFolder(folderPath: String) {
        try {
            // 删除完里面所有内容
            delAllFile(folderPath)
            val myFilePath = File(folderPath)
            // 删除空文件夹
            myFilePath.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除指定文件夹下所有文件
     *
     * @param path 文件夹完整绝对路径
     * @return true/false
     */
    private fun delAllFile(path: String): Boolean {
        var flag = false
        val file = File(path)
        if (!file.exists()) {
            return false
        }
        if (!file.isDirectory) {
            return false
        }
        val tempList: Array<String>? = file.list()
        var temp: File
        if (null != tempList) {
            for (aTempList: String in tempList) {
                temp = if (path.endsWith(File.separator)) {
                    File(path + aTempList)
                } else {
                    File(path + File.separator + aTempList)
                }
                if (temp.isFile) {
                    temp.delete()
                }
                if (temp.isDirectory) {
                    // 先删除文件夹里面的文件
                    delAllFile("$path/$aTempList")
                    // 再删除空文件夹
                    delFolder("$path/$aTempList")
                    flag = true
                }
            }
        }
        return flag
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建文件夹
     *
     * @param path 文件夹路径
     *
     * @return 文件夹路径
     */
    @JvmStatic
    fun createPath(path: String): File {
        val file = File(path)
        try {
            if (!file.exists()) {
                file.mkdirs()
            }
        } catch (e: IOException) {
            throw BaseException(">>>>>>>>创建[$path]文件夹失败<<<<<<<<")
        }
        return file
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * dateFormat 格式日期
     *
     * @return yyyyMMdd
     */
    private fun getDateFormat(): String {
        return DateUtil.formatDate(Date(), DateUtil.TIME)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FileUtil class

/* End of file FileUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/FileUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
