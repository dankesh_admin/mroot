/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util


import org.apache.commons.collections.CollectionUtils
import javax.validation.ConstraintViolation

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.groups.Default

/**
 * Hibernate Validation 验证工具类
 *
 * @author ErYang
 */

object HibernateValidationUtil {

    /**
     * 验证信息连接标识
     */
    const val CONNECT_CHAR = "@"

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 Validator 实例
     */
    private val VALIDATOR: Validator = Validation.buildDefaultValidatorFactory().validator

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类
     *
     * @param t 实体类
     * @return 验证信息
     */
    fun <T> validateEntity(t: T): String? {
        // 用于存储验证后的错误信息
        val stringBuffer = StringBuilder()
        val set: Set<ConstraintViolation<T>> = VALIDATOR.validate(t, Default::class.java)
        if (CollectionUtils.isNotEmpty(set)) {
            for (constraintViolation: ConstraintViolation<T> in set) {
                stringBuffer.append(constraintViolation.message)
                stringBuffer.append(CONNECT_CHAR)
            }
        }
        return stringBuffer.toString()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类
     * 分组验证
     *
     * @param t 实体类
     * @return 验证信息
     */
    fun <T> validateEntity(t: T, vararg var1: Class<*>): String? {
        // 用于存储验证后的错误信息
        val stringBuffer = StringBuilder()
        val set: Set<ConstraintViolation<T>> = VALIDATOR.validate(t, *var1)
        if (CollectionUtils.isNotEmpty(set)) {
            for (constraintViolation: ConstraintViolation<T> in set) {
                stringBuffer.append(constraintViolation.message)
                stringBuffer.append(CONNECT_CHAR)
            }
        }
        return stringBuffer.toString()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证实体类的某个字段
     *
     * @param t            实体类
     * @param propertyName 字段名称
     * @return 验证信息
     */
    fun <T> validateProperty(t: T, propertyName: String): String? {
        // 用于存储验证后的错误信息
        val stringBuffer = StringBuilder()
        val set: Set<ConstraintViolation<T>> = VALIDATOR.validateProperty(t, propertyName, Default::class.java)
        if (CollectionUtils.isNotEmpty(set)) {
            for (constraintViolation: ConstraintViolation<T> in set) {
                stringBuffer.append(constraintViolation.message)
                stringBuffer.append(CONNECT_CHAR)
            }
        }
        return stringBuffer.toString()
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HibernateValidationUtil class

/* End of file HibernateValidationUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/HibernateValidationUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

