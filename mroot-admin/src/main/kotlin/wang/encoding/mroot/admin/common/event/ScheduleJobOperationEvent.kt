/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.event


import org.springframework.context.ApplicationEvent
import wang.encoding.mroot.model.entity.system.ScheduleJob
import wang.encoding.mroot.model.enums.ScheduleJobOperationTypeEnum


/**
 * 定时任务 操作事件
 *
 * @author ErYang
 */
class ScheduleJobOperationEvent
/**
 * 实例化
 *
 * @param scheduleJob       定时任务
 * @param scheduleJobOperationTypeEnum 定时任务操作枚举类
 */(scheduleJob: ScheduleJob, scheduleJobOperationTypeEnum: ScheduleJobOperationTypeEnum) : ApplicationEvent(scheduleJob) {

    companion object {
        private const val serialVersionUID = -4177097414400610528L
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *定时任务操作枚举类
     */
    var scheduleJobOperationTypeEnum: ScheduleJobOperationTypeEnum? = scheduleJobOperationTypeEnum


    override fun toString(): String {
        return "ScheduleJobOperationEvent(scheduleJobOperationTypeEnum=$scheduleJobOperationTypeEnum)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobOperationEvent class

/* End of file ScheduleJobOperationEvent.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/event/ScheduleJobOperationEvent.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------
